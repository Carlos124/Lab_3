package arrayIndexList;

import indexList.IndexList;

public class ArrayIndexList<E> implements IndexList<E> {
	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size; 

	public ArrayIndexList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 
	
	private ArrayIndexList(E[] arr, int s) {
		element = arr;
		size = s;
	}

	public void add(int index, E e) throws IndexOutOfBoundsException {
		// ADD CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>this.size())) throw new IndexOutOfBoundsException("add: invalid index = "+index);
		if(index==this.size()){
			this.add(e);
			return;
		}
		if(this.size()==this.element.length){
			this.changeCapacity(CAPTOAR);
		}

		this.moveDataOnePositionTR(index, size-1);
		this.element[index]=e;
		this.size++;
	}


	public void add(E e) {
		// ADD CODE AS REQUESTED BY EXERCISES
		if(this.size()==this.element.length){
			this.changeCapacity(CAPTOAR);
			
		}
		this.element[this.size++]=e;
	}


	public E get(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIGY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>this.size())) throw new IndexOutOfBoundsException();
		return this.element[index]; 
	}


	public boolean isEmpty() {
		return this.size() == 0;
	}


	public E remove(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>this.size()-1)) throw new IndexOutOfBoundsException("remove: invalid index = "+index);
		E result=this.element[index];
		this.moveDataOnePositionTL(index+1, this.size()-1);
		this.element[--this.size]=null;
		return result;
		
		
	}


	public E set(int index, E e) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>this.size())) throw new IndexOutOfBoundsException("set: invalid index = "+index);
		E result=this.element[index];
		this.element[index]=e;
		return result;
	}


	public int size() {
		return this.size;
	}	
	public int capacity() { 
	     return element.length; 
	}

	
	
	
	// private methods  -- YOU CAN NOT MODIFY ANY OF THE FOLLOWING
	// ... ANALYZE AND USE WHEN NEEDED
	
	// you should be able to decide when and how to use
	// following method.... BUT NEED TO USE THEM WHENEVER
	// NEEDED ---- THIS WILL BE TAKEN INTO CONSIDERATION WHEN GRADING
	
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	// useful when adding a new element with the add
	// with two parameters....
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	// useful when removing an element from the list...
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}


	// The following two methods are to be implemented as part of an exercise
	public Object[] toArray() {
		Object [] arr=new Object [size()];
		int i=0;
		for(E e:element)
			arr[i++]=e;
		return arr;
	}


	@Override
	public <T1> T1[] toArray(T1[] array) {
			
		return array.clone();
	}
	
	public void clear() {
		element = (E[]) new Object[element.length];
		size = 0;
	}
	
	public ArrayIndexList clone () {
		return new ArrayIndexList(element.clone(),size);
	}
	public ArrayIndexList sub(int FirstIndex,int LastIndex) {
		E[] el = (E[]) new Object[LastIndex-FirstIndex+1];
		short x = 0;
		for(int i=FirstIndex; i<=LastIndex; i++) {
			el[x++]=element[i];
		}
		return new ArrayIndexList(el, el.length);
	}
}
